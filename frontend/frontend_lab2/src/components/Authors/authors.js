// import React from 'react';
import React, {Component} from "react";
//import BookTerm from "../BookTerm/bookTerm";
//import {Link} from "react-router-dom";
import ReactPaginate  from "react-paginate";

class Authors extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page:0,
            size:5
        }
    }
    getAuthorsPage(offset, nextPageOffset){
        return this.props.authors.map((term)=>{
            return (
                /*<BookTerm key={term.id}  term={term} onDelete={this.props.onDelete} onEdit={this.props.onEdit}
                onMarkAsTaken={this.props.onMarkAsTaken}/>*/
                /*<Author key={term.id}  term={term} onDelete={this.props.onDelete} onEdit={this.props.onEdit}
                onMarkAsTaken={this.props.onMarkAsTaken}/>*/
                <tr key={term.id}>
                <td>{term.name}</td>
                <td>{term.surname}</td>
                <td>{term.country.name}</td>
                </tr>
            );
        }).filter((author, index)=>{
            return index>=offset && index<nextPageOffset;
        });
    }
    handlePageClick = (data) => {
        let selected = data.selected;
        this.setState({
            page:selected
        })
    }
    render(){
        const offset = this.state.page * this.state.size;
        const nextPageOffset =offset + this.state.size;
        const pageCount = Math.ceil(this.props.authors.length / this.state.size);
        const authors = this.getAuthorsPage(offset, nextPageOffset);
        return (
            <div className={"container-fluid"}>
                <hr/>
                <h1 className={"text-center"}>Authors</h1>
                <hr/>
                <div>
                    <table className={"table table-striped"}>
                        <thead>
                        <tr>
                            <th scope={"col"}>Name</th>
                            <th scope={"col"}>Surname</th>
                            <th scope={"col"}>Country</th>
                        </tr>
                        </thead>
                        <tbody>
                        {authors}
                        </tbody>
                    </table>
                    <ReactPaginate previousLabel={"back"}
                                   nextLabel={"next"}
                                   breakLabel={<a href="/#">...</a>}
                                   breakClassName={"break-me"}
                                   pageClassName={"ml-1"}
                                   pageCount={pageCount}
                                   marginPagesDisplayed={2}
                                   pageRangeDisplayed={5}
                                   onPageChange={this.handlePageClick}
                                   containerClassName={"pagination m-4 justify-content-center"}
                                   subContainerClassName={"pages pagination"}
                                   activeClassName={"active"}/>
                </div>
            </div>
        );
    }
}
export default Authors;