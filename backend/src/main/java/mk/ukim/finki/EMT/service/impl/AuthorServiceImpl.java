package mk.ukim.finki.EMT.service.impl;

import mk.ukim.finki.EMT.model.Author;
import mk.ukim.finki.EMT.repository.AuthorRepository;
import mk.ukim.finki.EMT.repository.CountryRepository;
import mk.ukim.finki.EMT.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final CountryRepository countryRepository;

    public AuthorServiceImpl(AuthorRepository authorRepository, CountryRepository countryRepository) {
        this.authorRepository = authorRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Author> findAll() {
        return authorRepository.findAll();
    }

    @Override
    public Optional<Author> findById(Long id) {
        return authorRepository.findById(id);
    }

    @Override
    public Optional<Author> save(String name, String surname, Long countryId) {
        Author author = new Author(name, surname, countryRepository.findById(countryId).orElseThrow(RuntimeException::new));
        return Optional.of(authorRepository.save(author));
    }

    @Override
    public Optional<Author> edit(Long id, String name, String surname, Long countryId) {
        Author author = authorRepository.findById(id).orElseThrow(RuntimeException::new);
        author.setName(name);
        author.setSurname(surname);
        author.setCountry(countryRepository.findById(countryId).orElseThrow(RuntimeException::new));
        return Optional.of(authorRepository.save(author));
    }

    @Override
    public void deleteById(Long id) {
        authorRepository.deleteById(id);
    }
}
