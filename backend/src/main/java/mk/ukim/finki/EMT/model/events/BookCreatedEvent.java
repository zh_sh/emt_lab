package mk.ukim.finki.EMT.model.events;

import lombok.Getter;
import mk.ukim.finki.EMT.model.Book;
import org.springframework.context.ApplicationEvent;

import java.time.Clock;
import java.time.LocalDateTime;

@Getter
public class BookCreatedEvent extends ApplicationEvent {
    private LocalDateTime when;
    public BookCreatedEvent(Book source) {
        super(source);
        this.when = LocalDateTime.now();
    }

//    public BookCreatedEvent(Book source, Clock clock) {
//        super(source, clock);
//    }
    public BookCreatedEvent(Book source, Clock clock) {
        super(source, clock);
    }
}
