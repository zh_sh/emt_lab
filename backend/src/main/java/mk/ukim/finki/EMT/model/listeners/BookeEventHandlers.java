package mk.ukim.finki.EMT.model.listeners;

import mk.ukim.finki.EMT.model.events.BookCreatedEvent;
import mk.ukim.finki.EMT.service.BookService;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class BookeEventHandlers {
    private final BookService bookService;

    public BookeEventHandlers(BookService bookService) {
        this.bookService = bookService;
    }

    @EventListener
    public void onBookCreated(BookCreatedEvent bookCreatedEvent){
//        System.out.println("book created");
//        bookCreatedEvent.notify();
        this.bookService.refreshMaterializedView();
    }
}
