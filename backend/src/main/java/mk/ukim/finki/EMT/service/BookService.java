package mk.ukim.finki.EMT.service;

import mk.ukim.finki.EMT.model.Book;
import mk.ukim.finki.EMT.model.Category;

import java.util.List;
import java.util.Optional;

public interface BookService {
    List<Book> findAll();

    Optional<Book> findById(Long id);

    Optional<Book> save(String name, Category category, Long authorId, Integer availableCopies);

    Optional<Book> edit(Long id, String name, Category category, Long authorId, Integer availableCopies);

    void markAsTaken(Long id);

    void deleteById(Long id);
    void refreshMaterializedView();
}
