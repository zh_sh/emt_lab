package mk.ukim.finki.EMT.service.impl;

import mk.ukim.finki.EMT.model.Author;
import mk.ukim.finki.EMT.model.Book;
import mk.ukim.finki.EMT.model.Category;
import mk.ukim.finki.EMT.model.events.BookCreatedEvent;
import mk.ukim.finki.EMT.repository.AuthorRepository;
import mk.ukim.finki.EMT.repository.BookRepository;
import mk.ukim.finki.EMT.service.BookService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public BookServiceImpl(BookRepository bookRepository, AuthorRepository authorRepository, ApplicationEventPublisher applicationEventPublisher) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public List<Book> findAll() {
        return this.bookRepository.findAll();
    }

    @Override
    public Optional<Book> findById(Long id) {
        return this.bookRepository.findById(id);
    }

    @Override
    public Optional<Book> save(String name, Category category, Long authorId, Integer availableCopies) {
        Book book = new Book(name, category, authorRepository.findById(authorId).get(), availableCopies);
//        System.out.println("book created");
        applicationEventPublisher.publishEvent(new BookCreatedEvent(book));
        return Optional.of(bookRepository.save(book));
    }

    @Override
    public Optional<Book> edit(Long id, String name, Category category, Long authorId, Integer availableCopies) {
        Book book = bookRepository.findById(id).orElseThrow(RuntimeException::new);

        book.setName(name);
        book.setCategory(category);
        book.setAuthor(authorRepository.findById(authorId).get());
        book.setAvailableCopies(availableCopies);

        return Optional.of(bookRepository.save(book));
    }

    @Override
    public void markAsTaken(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(RuntimeException::new);
        book.setAvailableCopies(book.getAvailableCopies() - 1);
        bookRepository.save(book);
    }

    @Override
    public void deleteById(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public void refreshMaterializedView() {
        System.out.println("book created!");
    }
}
