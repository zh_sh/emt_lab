package mk.ukim.finki.EMT.web.rest;

import mk.ukim.finki.EMT.model.Country;
import mk.ukim.finki.EMT.model.Dto.CountryDto;
import mk.ukim.finki.EMT.service.CountryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:3001", "http://localhost:9091"})
@CrossOrigin("*")
@RequestMapping("/api/countries")
public class CountryRestController {

    private final CountryService countryService;

    public CountryRestController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping(value = {"", "/"})
    public ResponseEntity<List<Country>> findAll() {
        return ResponseEntity.ok(this.countryService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Country> findById(@PathVariable Long id){
        return this.countryService.findById(id)
                .map(author -> ResponseEntity.ok().body(author))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

//    @PostMapping("/add")
//    public ResponseEntity<Country> save(@RequestParam(name = "name") String name,
//                                        @RequestParam(name = "continent") String continent){
//        return this.countryService.save(name, continent)
//                .map(author -> ResponseEntity.ok().body(author))
//                .orElseGet(() -> ResponseEntity.notFound().build());
//    }

    @PostMapping(value = {"/add", "/add/"})
    public ResponseEntity<Country> save(@RequestBody CountryDto countryDto){
        return this.countryService.save(countryDto.getName(), countryDto.getContinent())
                .map(country -> ResponseEntity.ok().body(country))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(value = {"/add", "/add/"})
    public ResponseEntity<Country> save2(@RequestParam(name = "name") String name,
                                        @RequestParam(name = "continent") String continent){
        return this.countryService.save(name, continent)
                .map(author -> ResponseEntity.ok().body(author))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

//    @PostMapping("/edit/{id}")
//    public ResponseEntity<Country> edit(@PathVariable Long id,
//                                        @RequestParam String name,
//                                        @RequestParam String continent){
//        Country country = countryService.edit(id, name, continent).get();
//        return ResponseEntity.ok().body(country);
//    }

    @PostMapping("/edit/{id}")
    public ResponseEntity<Country> edit(@PathVariable Long id,
                                        @RequestBody CountryDto countryDto){
        Country country = countryService.edit(id, countryDto.getName(), countryDto.getContinent()).get();
        return ResponseEntity.ok().body(country);
    }

    @GetMapping("/edit/{id}")
    public ResponseEntity<Country> edit2(@PathVariable Long id,
                                        @RequestParam String name,
                                        @RequestParam String continent){
        Country country = countryService.edit(id, name, continent).get();
        return ResponseEntity.ok().body(country);
    }
}
