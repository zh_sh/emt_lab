package mk.ukim.finki.EMT.web.rest;

import mk.ukim.finki.EMT.model.Author;
import mk.ukim.finki.EMT.model.Dto.AuthorDto;
import mk.ukim.finki.EMT.service.AuthorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:3001", "http://localhost:9091"})
@CrossOrigin("*")
@RequestMapping("/api/authors")
public class AuthorRestController {
    private final AuthorService authorService;

    public AuthorRestController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping(value = {"", "/"})
    public ResponseEntity<List<Author>> findAll() {
        return ResponseEntity.ok(this.authorService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Author> findById(@PathVariable Long id){
        return this.authorService.findById(id)
                .map(author -> ResponseEntity.ok().body(author))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

//    @PostMapping("/add")
//    public ResponseEntity<Author> save(@RequestParam String name,
//                                       @RequestParam String surname,
//                                       @RequestParam Long countryId){
//        return this.authorService.save(name, surname, countryId)
//                .map(author -> ResponseEntity.ok().body(author))
//                .orElseGet(() -> ResponseEntity.notFound().build());
//    }

    @PostMapping("/add")
    public ResponseEntity<Author> save(@RequestBody AuthorDto authorDto){
        return this.authorService.save(authorDto.getName(), authorDto.getSurname(), authorDto.getCountryId())
                .map(author -> ResponseEntity.ok().body(author))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(value = {"/add", "/add/"})
    public ResponseEntity<Author> save2(@RequestParam String name,
                                       @RequestParam String surname,
                                       @RequestParam Long countryId){
        return this.authorService.save(name, surname, countryId)
                .map(author -> ResponseEntity.ok().body(author))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

//    @PostMapping("/edit/{id}")
//    public ResponseEntity<Author> edit(@PathVariable Long id,
//                                       @RequestParam String name,
//                                       @RequestParam String surname,
//                                       @RequestParam Long countryId){
//        Author author = authorService.edit(id, name, surname, countryId).get();
//        return ResponseEntity.ok().body(author);
//    }

    @PostMapping("/edit/{id}")
    public ResponseEntity<Author> edit(@PathVariable Long id,
                                       @RequestBody AuthorDto authorDto){
        Author author = authorService.edit(id, authorDto.getName(), authorDto.getSurname(), authorDto.getCountryId()).get();
        return ResponseEntity.ok().body(author);
    }

    @GetMapping("/edit/{id}")
    public ResponseEntity<Author> edit2(@PathVariable Long id,
                                       @RequestParam String name,
                                       @RequestParam String surname,
                                       @RequestParam Long countryId){
        Author author = authorService.edit(id, name, surname, countryId).get();
        return ResponseEntity.ok().body(author);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Author> deleteById(@PathVariable Long id){

        this.authorService.deleteById(id);

        if(this.authorService.findById(id).isEmpty()){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build();
        }
    }

}
