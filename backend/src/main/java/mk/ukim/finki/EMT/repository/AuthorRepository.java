package mk.ukim.finki.EMT.repository;

import mk.ukim.finki.EMT.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository <Author, Long>{
}
