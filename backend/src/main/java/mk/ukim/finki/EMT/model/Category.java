package mk.ukim.finki.EMT.model;

public enum Category {
    NOVEL, THRILER, HISTORY, FANTASY, BIOGRAPHY, CLASSICS, DRAMA
}
