package mk.ukim.finki.EMT.web.rest;

import mk.ukim.finki.EMT.model.Book;
import mk.ukim.finki.EMT.model.Category;
import mk.ukim.finki.EMT.model.Dto.BookDto;
import mk.ukim.finki.EMT.service.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:3001", "http://localhost:9091"})
@CrossOrigin("*")
@RequestMapping("/api/books")
public class BookRestController {

    private final BookService bookService;

    public BookRestController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(value = {"", "/"})
    public ResponseEntity<List<Book>> findAll(){
        return ResponseEntity.ok(bookService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> findById(@PathVariable Long id){
        return this.bookService.findById(id)
                .map(book -> ResponseEntity.ok().body(book))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /*@RequestParam(name = "category", required = false)*/
    //    @PostMapping("/add")
//    public ResponseEntity<Book> save(@RequestParam String name,
//            /*@RequestParam Category category,*/
//                                     @RequestParam String category,
//                                     @RequestParam Long authorId,
//                                     @RequestParam Integer availableCopies){
////        return this.bookService.save(name, category, authorId, availableCopies)
//        return this.bookService.save(name, Category.valueOf(category), authorId, availableCopies)
//                .map(book -> ResponseEntity.ok().body(book))
//                .orElseGet(() -> ResponseEntity.notFound().build());
//    }

    @PostMapping(value = {"/add", "/add/"})
    public ResponseEntity<Book> save(@RequestBody BookDto bookDto){
//        return this.bookService.save(name, category, authorId, availableCopies)
//        return this.bookService.save(name, Category.valueOf(category), authorId, availableCopies)
        return this.bookService.save(bookDto.getName(), Category.valueOf(bookDto.getCategory()), bookDto.getAuthorId(), bookDto.getAvailableCopies())
                .map(book -> ResponseEntity.ok().body(book))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(value = {"/add", "/add/"})
    public ResponseEntity<Book> save2(@RequestParam String name,
                                     /*@RequestParam Category category,*/
                                      @RequestParam String category,
                                      @RequestParam Long authorId,
                                      @RequestParam Integer availableCopies){
//        return this.bookService.save(name, category, authorId, availableCopies)
        return this.bookService.save(name, Category.valueOf(category), authorId, availableCopies)
                .map(book -> ResponseEntity.ok().body(book))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

//    @PostMapping("/edit/{id}")
//    public ResponseEntity<Book> edit(@PathVariable Long id,
//                                     @RequestParam String name,
//                                     /*@RequestParam Category category,*/
//                                     @RequestParam String category,
//                                     @RequestParam Long authorId,
//                                     @RequestParam Integer availableCopies){
//        Book book = bookService.edit(id, name, Category.valueOf(category), authorId, availableCopies).get();
//        return ResponseEntity.ok().body(book);
//    }

    @PostMapping("/edit/{id}")
    public ResponseEntity<Book> edit(@PathVariable Long id,
                                     @RequestBody BookDto bookDto){
        Book book = bookService.edit(id, bookDto.getName(), Category.valueOf(bookDto.getCategory()), bookDto.getAuthorId(), bookDto.getAvailableCopies()).get();
        return ResponseEntity.ok().body(book);
    }

    @GetMapping("/edit/{id}")
    public ResponseEntity<Book> edit2(@PathVariable Long id,
                                     @RequestParam String name,
                                     /*@RequestParam Category category,*/
                                     @RequestParam String category,
                                     /*@RequestParam Long authorId,*/
                                     @RequestParam String authorId,
                                     /*@RequestParam Integer availableCopies*/
                                     @RequestParam String availableCopies
    ){
//        Book book = bookService.edit(id, name, category, authorId, availableCopies).get();
//        Book book = bookService.edit(id, name, Category.valueOf(category), authorId, availableCopies).get();
        Book book = bookService.edit(id, name, Category.valueOf(category), Long.valueOf(authorId), Integer.valueOf(availableCopies)).get();
        return ResponseEntity.ok().body(book);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Book> deleteById(@PathVariable Long id){

        this.bookService.deleteById(id);

        if(this.bookService.findById(id).isEmpty()){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping(value = {"/markAsTaken/{id}", "/markAsTaken/{id}/"})
    public void markAsTaken(@PathVariable Long id) {
        this.bookService.markAsTaken(id);
    }

}
